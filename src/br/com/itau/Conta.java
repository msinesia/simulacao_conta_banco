package br.com.itau;

public class Conta {
    private int num_conta;
    private int ag_conta;
    private int tp_conta;
    private double saldo;
    private int limite;
    private Cliente cliente;

    //construtores - overloading
    public Conta(){}

    //cliente sem limite
    public Conta(int num_conta, int ag_conta, int tp_conta, double saldo, Cliente cliente){
        this.num_conta = num_conta;
        this.ag_conta = ag_conta;
        this.tp_conta = tp_conta;
        this.saldo = saldo;
        this.cliente = cliente;
    }

    //cliente com limite
    public Conta(int num_conta, int ag_conta, int tp_conta, double saldo, int limite, Cliente cliente){
        this.num_conta = num_conta;
        this.ag_conta = ag_conta;
        this.tp_conta = tp_conta;
        this.saldo = saldo;
        this.cliente = cliente;
    }

    // getters e setters acesso aos atributos encapsulados (private)
    public int getNum_conta(){
        return num_conta;
    }

    public void setNum_conta(int num_conta){
        this.num_conta = num_conta;
    }

    public int getAg_conta() {
        return ag_conta;
    }

    public void setAg_conta(int ag_conta) {
        this.ag_conta = ag_conta;
    }

    public int getTp_conta() {
        return tp_conta;
    }

    public void setTp_conta(int tp_conta) {
        this.tp_conta = tp_conta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public int getLimite() {
        return limite;
    }

    public void setLimite(int limite) {
        this.limite = limite;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    //métodos

    public void sacar (double valor){
        if(saldo > valor){
            saldo = saldo - valor;
        }
        else if((limite + saldo) > valor){


        }

    }

    public
}
