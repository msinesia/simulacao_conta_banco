package br.com.itau;

import java.util.Date;

public class Cliente {
    private String cpf;
    private String nome;
    private int idade;

    //construtores - overloading

    public Cliente(String cpf, String nome, int idade) {
        this.cpf = cpf;
        this.nome = nome;
        this.idade = idade;
    }

    //encapsulamento
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    //validação idade do cliente
    public boolean validarIdade(){
        if (idade >= 18){
            return true;
        }
        return false;
    }
}
